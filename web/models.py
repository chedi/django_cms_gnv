from django.db import models
from transmeta import TransMeta
from cms.models import CMSPlugin
from filer.fields.image import FilerImageField
from django.utils.translation import ugettext as _


class DealsCollection(models.Model):
    __metaclass__ = TransMeta

    title = models.CharField(max_length=200, verbose_name=_("Title"))

    def __unicode__(self):
        return self.title

    class Meta:
        translate = ('title',)


class TopicsCollection(models.Model):
    __metaclass__ = TransMeta

    title = models.CharField(max_length=200, verbose_name=_("Title"))

    def __unicode__(self):
        return self.title

    class Meta:
        translate = ('title',)


class Deal(models.Model):
    __metaclass__ = TransMeta

    title       = models.CharField(max_length=200, verbose_name=_("Title"))
    caption     = models.CharField(max_length=200, verbose_name=_("Caption"))
    url         = models.CharField(max_length=300, verbose_name=_("url"))
    order       = models.PositiveIntegerField()
    price       = models.PositiveIntegerField(verbose_name=_("Price"))
    end_date    = models.DateField(null=True, blank=True, verbose_name=_("End date"))
    start_date  = models.DateField(null=True, blank=True, verbose_name=_("Start date"))
    image       = FilerImageField(verbose_name=_("Image"))
    description = models.TextField(verbose_name=_("Description"))
    collection  = models.ForeignKey(DealsCollection, verbose_name=_("Deals collection"))
    published   = models.BooleanField(default=True, verbose_name=_("Published"))
    sidebar     = models.BooleanField(default=False, verbose_name=_("Sidebar"))

    class Meta:
        verbose_name_plural = _("Deals")
        translate = ('title', 'caption', 'description',)
        ordering = ['order']

    def __unicode__(self):
        return self.title


class Topic(models.Model):
    __metaclass__ = TransMeta

    title       = models.CharField(max_length=200, verbose_name=_("Title"))
    url         = models.CharField(max_length=300, blank=True, null=True, verbose_name=_("url"))
    order       = models.PositiveIntegerField()
    image       = FilerImageField(verbose_name=_("Image"))
    description = models.TextField(null=True, blank=True, verbose_name=_("Description"))
    collection  = models.ForeignKey(TopicsCollection, verbose_name=_("Topics collection"))
    published   = models.BooleanField(default=True, verbose_name=_("Published"))

    class Meta:
        verbose_name_plural = _("Topics")
        translate = ('title', 'description',)
        ordering = ['order']

    def __unicode__(self):
        return self.title


class DealsPluginModel(CMSPlugin):
    deals = models.ForeignKey(DealsCollection)

    def __unicode__(self):
        return self.deals.title


class TopicPluginModel(CMSPlugin):
    topics = models.ForeignKey(TopicsCollection)

    def __unicode__(self):
        return self.topics.title


class News(models.Model):
    __metaclass__ = TransMeta

    title       = models.CharField(max_length=200, verbose_name=_("Title"))
    url         = models.CharField(max_length=300, verbose_name=_("url"))
    image       = FilerImageField(verbose_name=_("Image"))
    order       = models.PositiveIntegerField()
    caption     = models.CharField(max_length=200, verbose_name=_("Caption"))
    description = models.TextField(verbose_name=_("Description"))
    published   = models.BooleanField(default=True, verbose_name=_("Published"))
    sidebar     = models.BooleanField(default=False, verbose_name=_("Sidebar"))

    class Meta:
        verbose_name_plural = _("News")
        translate = ('title', 'caption', 'description',)
        ordering = ['order']

    def __unicode__(self):
        return self.title


class NewsPluginModel(CMSPlugin):
    title = models.CharField(max_length=200)

    def __unicode__(self):
        return self.title
