from django import conf
from django.contrib import admin
from web.views import LandingPage
from cms.sitemaps import CMSSitemap
from web.views import WebServiceDetail, NewsletterCOS
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns(
    '',
    #url(r'^[/]?$', LandingPage),
    url(r'^search/', include('haystack.urls')),
    url(r'^sidebar/', WebServiceDetail.as_view()),
    url(r'^jsi18n/(?P<packages>\S+?)/$', 'django.views.i18n.javascript_catalog'),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^newslettercos/$', NewsletterCOS.as_view()),
    url(r'^', include('cms.urls')),
)

urlpatterns += staticfiles_urlpatterns()

if conf.settings.DEBUG:
    urlpatterns = patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': conf.settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns
